let page = {};
let visible = true;

page.init = function() {
    let bars = document.getElementById('btn-bars');
    let txtSidebar = document.getElementsByClassName('txt-sidebar');
    let mainSidebar = document.getElementsByClassName('main-sidebar');
    let mainHeader =  document.getElementsByClassName('main-header');
    let dropdown = document.getElementsByClassName('btn-dropdown');
    let contentHover = document.getElementsByClassName('content-hover');
    let sidebarOverlay = document.getElementById('sidebar-overlay');
    let x = window.matchMedia("(min-width: 1024px)");

    bars.onclick = function () {
        if (visible){
            if (x.matches) { // If media query matches
                addClassForLargeScreen();
            } else {
                addClassForMiniScreen();
            }

            visible = !visible;
        } else {
            if (x.matches) { // If media query matches
                removeClassForLargeScreen();
            }

            visible = !visible;
        }
    }

    window.addEventListener('resize', function () {
        if (window.innerWidth >= 1024) {
            removeClassForMiniScreen();
        } else {
            removeClassForLargeScreen();
        }
    });

    sidebarOverlay.addEventListener('click', function () {
        removeClassForMiniScreen();
        visible = !visible;
    });
    
    for (let i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener('click', function () {
            this.classList.toggle('active');
            let dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === 'none') {
                this.children[1].children[1].classList.add('rotate-icon-menu');
                dropdownContent.style.display = 'block';
            } else {
                this.children[1].children[1].classList.remove('rotate-icon-menu');
                dropdownContent.style.display = 'none';
            }
        })
    }

    // add class for large screen
    function addClassForLargeScreen() {
        for (let i = 0; i < txtSidebar.length; i++) {
            txtSidebar[i].style.opacity = '0%';
            txtSidebar[i].style.visibility = 'hidden';
        }

        for (let i = 0; i < mainHeader.length; i++) {
            mainHeader[i].classList.add('hidden-left');
        }

        for (let i = 0; i < mainSidebar.length; i++) {
            mainSidebar[i].classList.add('hidden-left-sidebar');
        }

        for (let i = 0; i < contentHover.length; i++) {
            contentHover[i].classList.add('content-hover-disable');
        }
    }

    // add class for mini screen
    function addClassForMiniScreen() {
        for (let i = 0; i < mainSidebar.length; i++) {
            mainSidebar[i].classList.add('main-sidebar-f');
        }

        sidebarOverlay.style.display = 'block';
    }

    // remove class for large screen
    function removeClassForLargeScreen() {
        for (let i = 0; i < txtSidebar.length; i++) {
            txtSidebar[i].style.opacity = '100%';
            txtSidebar[i].style.visibility = 'visible';
        }

        for (let i = 0; i < mainHeader.length; i++) {
            mainHeader[i].classList.remove('hidden-left');
        }

        for (let i = 0; i < mainSidebar.length; i++) {
            mainSidebar[i].classList.remove('hidden-left-sidebar');
        }

        for (let i = 0; i < contentHover.length; i++) {
            contentHover[i].classList.remove('content-hover-disable');
        }
    }

    function removeClassForMiniScreen() {
        for (let i = 0; i < mainSidebar.length; i++) {
            mainSidebar[i].classList.remove('main-sidebar-f');
        }
        sidebarOverlay.style.display = 'none';
    }
}

window.onload = function () {
    page.init();
}
